<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

// HOME
Route::get('/', array('uses' => 'IndexController@showIndex', 'as' => 'index'));

// SETTINGS
Route::post('/saveSettings', array('uses' => 'SettingsController@saveSettings', 'as' => 'saveSettings'));
Route::get('/settings', array('uses' => 'SettingsController@showSettings', 'as' => 'settings'));

// LOGS
Route::any('/logs-main', array('uses' => 'LogsController@showLogs', 'as' => 'logs-main'));//

// Confide routes
Route::get('users/create', 'UsersController@create');
Route::post('users', 'UsersController@store');
Route::get('users/login',  'UsersController@login');
Route::post('users/login', array('uses' =>'UsersController@doLogin' , 'as' => 'login'));
Route::get('users/confirm/{code}', 'UsersController@confirm');
Route::get('users/forgot_password', 'UsersController@forgotPassword');
Route::post('users/forgot_password', 'UsersController@doForgotPassword');
Route::get('users/reset_password/{token}', 'UsersController@resetPassword');
Route::post('users/reset_password', 'UsersController@doResetPassword');
Route::get('users/logout', 'UsersController@logout');
