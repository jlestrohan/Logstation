<?php
class Helpers {

    /**
     * Renvoie un label bootstrap avec le libelle de la severity en fonction du niveau entré en argument
     * @param $level
     *
     * @return string
     */
    public static function showSeverityStamp($level) {

        $var1 = '';
        $sev  = '';

        switch ($level) {
            case "0":
                $sev = "EMERG";
                $var1 = 'danger';
                break;

            case "1":
                $sev = "ALERT";
                $var1 = "danger";
                break;

            case "2":
                $sev = "CRITICAL";
                $var1 = "danger";
                break;

            case "3":
                $sev = "ERROR";
                $var1 = "danger";
                break;

            case "4":
                $sev = "WARNING";
                $var1 = "danger";
                break;

            case "5":
                $sev = "NOTICE";
                $var1 = "warning";
                break;

            case "6":
                $sev = "INFO";
                $var1 = "primary";
                break;

            case "7":
                $sev = "DEBUG";
                $var1 = "success";
                break;

        }
        return '<span class="label label-' . $var1  . '">' . $sev  . '</span>';
    }

    /**
     * Formate la date pour affichage
     * @param $datestring
     *
     * @return string
     */
    public static function formatDate ($datestring)
    {
        $dt = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$datestring)->toDateTimeString();

        return $dt;

    }

    /**
     * retourne une chaine HTML représentant un label bootstrap + nom facility en échange d'un entier en argument
     * cf http://en.wikipedia.org/wiki/Syslog
     * @param $level
     *
     * @return string
     */
    public static function showFacilityStamp($facility) {

        $sev = '';
        $var1 = '';
        switch ($facility) {
            case "0": // kernel messages
                $sev = "kern";
                $var1 = 'danger';
                break;

            case "1": // user-level messages
                $sev = "user";
                $var1 = "danger";
                break;

            case "2": // mail system
                $sev = "mail";
                $var1 = "danger";
                break;

            case "3": // system daemons
                $sev = "daemon";
                $var1 = "warning";
                break;

            case "4": // security/authorization messages
                $sev = "auth";
                $var1 = "danger";
                break;

            case "5": // messages generated internally by syslogd
                $sev = "syslog";
                $var1 = "primary";
                break;

            case "6": // line printer subsystem
                $sev = "lpr";
                $var1 = "info";
                break;

            case "7": // network news subsystem
                $sev = "news";
                $var1 = "success";
                break;

            case "8": // UUCP subsystem
                $sev = "uucp";
                $var1 = 'danger';
                break;

            case "9": // clock daemon
                $sev = "cron";
                $var1 = "info";
                break;

            case "10": // security/authorization messages
                $sev = "authpriv";
                $var1 = "danger";
                break;

            case "11": // FTP daemon
                $sev = "ftp";
                $var1 = "danger";
                break;

            case "12": // NTP subsystem
                $sev = "-";
                $var1 = "warning";
                break;

            case "13": // log audit
                $sev = "-";
                $var1 = "primary";
                break;

            case "14": // log alert
                $sev = "-";
                $var1 = "info";
                break;

            case "15": // clock daemon
                $sev = "cron";
                $var1 = "success";
                break;

            case "16": // local use 0 (local0)
                $sev = "local0";
                $var1 = 'danger';
                break;

            case "17": // local use 1 (local1)
                $sev = "local1";
                $var1 = "danger";
                break;

            case "18": // local use 2 (local2)
                $sev = "local2";
                $var1 = "danger";
                break;

            case "19": // local use 3 (local3)
                $sev = "local3";
                $var1 = "danger";
                break;

            case "20": // local use 4 (local4)
                $sev = "local4";
                $var1 = "warning";
                break;

            case "21": // local use 5 (local5)
                $sev = "local5";
                $var1 = "primary";
                break;

            case "22": // local use 6 (local6)
                $sev = "local6";
                $var1 = "info";
                break;

            case "23": // local use 7 (local7)
                $sev = "local7";
                $var1 = "success";
                break;
        }
        return '<span class="label label-' . $var1  . '">' . strtoupper($sev)  . '</span>';
    }


    /**
     * EN CONSTRUCTION
     * Extrait les ip des messages et remplace
     * @param $string
     *
     * @return mixed
     */
    public static function formatExtraInfoString($string)
    {
       // IP:  preg_match("/(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/", $string, $matches);
        $string = preg_replace(
            array(
                '/(^|\s|>)(www.[^<> \n\r]+)/iex',
                '/(^|\s|>)([_A-Za-z0-9-]+(\\.[A-Za-z]{2,3})?\\.[A-Za-z]{2,4}\\/[^<> \n\r]+)/iex',
                '/(?(?=<a[^>]*>.+<\/a>)(?:<a[^>]*>.+<\/a>)|([^="\']?)((?:https?):\/\/([^<> \n\r]+)))/iex'
            ),
            array(
                "stripslashes((strlen('\\2')>0?'\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>&nbsp;\\3':'\\0'))",
                "stripslashes((strlen('\\2')>0?'\\1<a href=\"http://\\2\" target=\"_blank\">\\2</a>&nbsp;\\4':'\\0'))",
                "stripslashes((strlen('\\2')>0?'\\1<a href=\"\\2\" target=\"_blank\">\\3</a>&nbsp;':'\\0'))",
            ),
            $string
        );
        return $string;
    }
}