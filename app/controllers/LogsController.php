<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class LogsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Logs Controller
	|--------------------------------------------------------------------------

	*/

    public function __construct()
    {
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', array ('on' => 'post'));

        // 1) connecte a la database en retirant les infos de connection de la table 'servers'
        $server1 = Servers::firstOrFail();

        Config::set('database.connections.server1', array(
            'driver'    => 'mysql',
            'host'      => $server1->dbserver,
            'database'  => $server1->dbname,
            'username'  => $server1->dbuser,
            'password'  => $server1->dbpass,
            'charset'   => 'utf8',
            'collation' => 'utf8_unicode_ci',
            'prefix'    => '',
        ));

        try {
            $serverConn = DB::connection('server1');
        } catch (PDOException $exception) {
            return Response::make('Database error: ' . $exception->getCode() . ' - ' . $exception->getMessage());
        }
    }

    public function showLogs()
    {
        // nombre d'éléments à montrer par page
        if (Input::has('records-per-page')) {
            $page_display = Input::get('records-per-page');
        } else $page_display = 25; // nombre de records par defaut affichés par page

        print_r(Input::all());

        $query1 = SystemEvents::on('server1')->groupBy('FromHost')->lists('FromHost','ID');
        $query2 = SystemEvents::on('server1')->orderBy('id', 'desc')->distinct()->paginate($page_display);
        $query3 = SystemEvents::on('server1')->count();


        return View::make('logs.main', array(   'entries' => $query2,
                                                'totalcount' => $query3,
                                                'hosts' => $query1,
                                                'recordspage' => $page_display
        ));
    }

}