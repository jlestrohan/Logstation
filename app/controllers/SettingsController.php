<?php

use Illuminate\Database\Eloquent\ModelNotFoundException;

class SettingsController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Settings Controller
	|--------------------------------------------------------------------------

	*/

    public function __construct()
    {
        $this->beforeFilter('auth');
        $this->beforeFilter('csrf', array ('on' => 'post'));


    }

    public function showSettings()
    {

        $server = Servers::first();
        View::make('settings.settings', array( 'server' => $server));
    }

    /**
     * @return mixed
     */
    public function saveSettings()
    {
        $validation = Validator::make(
            array(
                'dbserver' => Input::get( 'dbserver' ),
                'dbname' => Input::get( 'dbname' ),
               'dbuser' => Input::get('dbuser'),
                'dbpass' => Input::get('dbpass'),
            ),
            array(
                'dbname' => array( 'required', 'alpha_dash' ),
                'dbuser' => array( 'required', 'alpha_dash'),
                'dbpass' => array( 'required'),
            )
        );

        if ($validation->fails())
        {
            return Redirect::back()->withErrors($validation)->withInput();
        }
        else
            // sauvegarde le modèle
            $serverSettings = new Servers();
            $serverSettings::truncate();
            $serverSettings->dbserver = Input::get( 'dbserver' );
            $serverSettings->dbname = Input::get( 'dbname' );
            $serverSettings->dbuser = Input::get( 'dbuser' );
            $serverSettings->dbpass = Input::get( 'dbpass' );

            $serverSettings->save();

            return Redirect::to('/');
    }

}
