@extends('layouts.master')

@section('content')



<div class="container-fluid">

<div class="row">

    <div class="col-xs-8 col-sm-2">
        @include('logs.sidebar')
    </div>


    <div class="col-xs-4 col-sm-10">
        <table class="table table-striped table-condensed table-bordered table-hover">
            <thead>
            <tr class="success">
                <th>Date</th>
                <th>Facility</th>
                <th>Severity</th>
                <th>Host</th>
                <th>Syslogtag</th>
                <th>Message</th>
            </tr>
            </thead>
            <tbody>

            @if (count($entries))
            @foreach($entries as $entry)
                @if ($entry->Priority <= 4)
                <tr class="warning">
                @else
                <tr class="vert-align">
                @endif
                <td nowrap><small>{{ Helpers::formatDate($entry->ReceivedAt) }}</small></td>
                <td><small>{{ Helpers::showFacilityStamp($entry->Facility) }}</small></td>
                <td><small>{{ Helpers::showSeverityStamp($entry->Priority) }}</small></td>
                <td><small><span class="label label-default">{{ strtoupper($entry->FromHost) }}</span></small></td>
                <td><small>{{ $entry->SysLogTag }}</small></td>
                <td><small>{{ Helpers::formatExtraInfoString($entry->Message) }}</small></td>
            </tr>
            @endforeach

            @endif
            </tbody>
        </table>
        <table class="table table-striped table-condensed table-bordered">
            <tr>
            <tr>
                <td colspan="100%" class="info">{{ $entries->links() }}</td>
            </tr>
        </table>
    </div>
</div>



@stop