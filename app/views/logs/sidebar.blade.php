@section('header')

{{ HTML::style('css/chosen.css') }}

@stop

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Statistics</h3>
    </div>
    <div class="panel-body">
        <ul class="nav nav-sidebar">
            <li>Records shown <span class="badge pull-right">{{ count($entries) }}</span></li>
            <li>Records total <span class="badge pull-right">{{ $totalcount }}</span></li>
        </ul>
    </div>
</div>


{{-- FILTERS --}}
<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">Filters</h3>
    </div>
    <div class="panel-body">
        <div class="side-by-side clearfix">
            <div>

                {{ Form::open(array('route' => 'logs-main')) }}

                <div class="form-group">
                    <em><label for="records-per-page">Click to select records-per-page</label></em>
                    <select data-placeholder="Pick a number..." class="recordspage-select" style="width:100%;" id="records-per-page" name="records-per-page">
                        <option value="25" @if ($recordspage <= 25) selected @endif>25</option>
                        <option value="50" @if ($recordspage == 50) selected @endif>50</option>
                        <option value="75" @if ($recordspage == 75) selected @endif>75</option>
                        <option value="100" @if ($recordspage == 100) selected @endif>100</option>
                        <option value="250" @if ($recordspage == 250) selected @endif>250</option>
                        <option value="500" @if ($recordspage >= 500) selected @endif>500</option>
                    </select>
                </div>

                <em><label for="multiple-label-example">Click to Highlight Multiple Select</label></em>
                <div class="form-group">
                <select data-placeholder="Please select a severity topic" multiple class="severity-select" style="width:100%;" tabindex="18" id="severity-pick" name="severity-pick[]">
                    <option value=""></option>
                    @for ($i = 0; $i < 8; $i++)
                    <option> {{ Helpers::showSeverityStamp($i) }}</option>
                    @endfor
                </select>
                    </div>

                <em><label for="multiple-label-example">Select Hosts</label></em>
                <div class="form-group">
                    {{ Form::select('hosts-pick[]', $hosts, Input::old('hosts-pick'), ['multiple' => true, 'class' => 'hosts-select', 'style' => 'width:100%;'] ) }}
                </div>

                <div class="form-group">
                {{ Form::submit('Apply filters', array('class' => 'btn btn-success btn-block')) }}
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</div>


{{-- SEARCH --}}
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Search</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <form class="form-inline" role="form">
                    <div class="form-group">
                        <label class="sr-only" for="searchField">Search</label>
                        <input type="email" class="form-control" id="searchField" placeholder="Search here...">
                    </div>

                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span>&nbsp;&nbsp;Go!</button>
                </form>
            </div>
        </div>
    </div>


@section('footer')

{{ HTML::style('css/chosen.css') }}
{{ HTML::script('js/chosen.jquery.min.js') }}
<script type="text/javascript">
    var config = {
        '.severity-select'           : {max_selected_options: 8},
        '.severity-select-deselect'  : {allow_single_deselect:true},
        '.severity-select-no-single' : {disable_search_threshold:10},
        '.severity-select-no-results': {no_results_text:'Oops, nothing found!'},
        '.severity-select-width'     : {width:"95%"},

        '.recordspage-select'       : {max_selected_options: 1},
        '.recordspage-select-deselect'  : {allow_single_deselect:false},

        '.hosts-select'       : {},
        '.hosts-select-deselect'  : {allow_single_deselect:false}
    }
    for (var selector in config) {
        $(selector).chosen(config[selector]);
    }
</script>
@stop

