<!doctype html>
<html lang="en">
<head>
    @include ('includes.head')
    @yield('header') {{-- pour tous les includes à venir en fonction des differentes views --}}
</head>

<body>

@include('includes.header')

@yield('content')

@include('includes.footer')
@yield('footer')
</body>
</html>