@extends('layouts.master')

@section('content')
<div class="bs-docs-header" id="content">
    <div class="container">
        <h1>Oops</h1>

        <p>The requested page could not be found...</p>
    </div>
</div>
<p align="center"><img border="0" height="420" src="/img/erreur404.jpg" width="598"/></p>
@stop