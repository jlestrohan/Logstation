@extends('layouts.master')

@section('content')
<div class="bs-docs-header" id="content">
    <div class="container">
        <h1>Logstation</h1>

        <p>The finest interface for all your logs survey needs...</p>
    </div>
</div>

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-4">
        <h2>Logstation is not operational yet!</h2>

        <p class="text-danger">As of today, this application is at his very early stage of development.</p>

        <p>It means that there is a very heavy ongoing work being done and the application is far from being operational yet.
            You may want to try loggin in if you want, but there's frankly not much interesting stuff to be discovered here.
            All information is stubbed, most of the routines do not work at all. It's still in development...!
            </p>

        <p><a class="btn btn-primary" href="#" role="button">View details &raquo;</a></p>
    </div>
    <div class="col-lg-3">
        <h2>When is that stuff shelved ?</h2>

        <p>Logstation will soon be released as alpha version on Github/Bitbucket opened repositories. Everyone is welcome to contribute as
        this is aimed to be a very open project. The idea behind this is to provide the network admins an efficient and free tool to monitor the logs of their networked
        machines the easy and fast way. Actually there are very few web interfaces around for such tasks, and offering another alternative is quite appealing. Stay tuned!</p>

        <p><a class="btn btn-primary" href="#" role="button">Contact me (soon avail)&raquo;</a></p>
    </div>
</div>

<div class="row">
    <div class="col-lg-3"></div>
    <div class="col-lg-8">
        <h2>Logstation is free and open-source!</h2>

        <p>But you are very welcome if you want to make a little donation to support the project and all the efforts
            that have been put into it.<br />
        Just click on the little button below and feel free to contribute, and many thanks in advance!</p>

        <p class="text-center">

        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="hosted_button_id" value="6PUL3KJN6N7Y4">
            <input type="image" src="https://www.paypalobjects.com/fr_XC/i/btn/btn_donateCC_LG.gif" border="0"
                   name="submit"
                   alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
            <img alt="" border="0" src="https://www.paypalobjects.com/fr_FR/i/scr/pixel.gif" width="1"
                 height="1">
        </form>
        </p>
    </div>
</div>


@stop