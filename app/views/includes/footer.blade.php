<footer>
    <hr/>
    <div class="col-md-6 col-md-offset-3"><p class="text-center">&copy; Jack Lestrohan - 2014</p></div>
    <div class="col-md-6 col-md-offset-3"><p class="text-center">All rights reserved</p></div>
    <div class="col-md-6 col-md-offset-3"><span class="text-center">
                    <ul class="bs-docs-footer-links muted">
                        <li>Version v0.1.0</li>

                    </ul>
            </p>

    </div>


</footer>

{{ HTML::script('//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js') }}
{{ HTML::script('//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js') }}




