<div class="navbar navbar-static-top navbar-inverse" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Logstation</a>
        </div>
        <div class="navbar-collapse collapse">

            @if (!Auth::check())
            {{ Form::open(['action' => 'login', 'class' => 'navbar-form navbar-right', 'role' => 'form']) }}
            <fieldset>
                <div class="form-group">
                    {{ Form::text('email',null, array('class' => 'form-control')) }}
                </div>
                <div class="form-group">
                    {{ Form::password('password', ['class' => 'form-control']) }}
                </div>
                {{ Form::submit('Sign in',['class'=>'btn btn-success']) }}
                </fieldset>
            {{ Form::close() }}

            @if($errors->has())
            @foreach ($errors->all() as $message)
            <span class="label alert round">{{$message}}</span><br><br>
            @endforeach
            @endif

            @if(Session::has('failure'))
            <span class="label alert round">{{Session::get('failure')}}</span>
            @endif

            @else
            {{-- navbar if logged in --}}
            <ul class="nav navbar-nav navbar-left">
                <li><a href="#">Dashboard</a></li>
                <li><a href="/logs-main#">Logs</a></li>
            </ul>

            <ul class="nav navbar-nav navbar-right">
                <li><a href="/settings">Settings</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome {{ Auth::user()->username }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="#"><p class=""navbar-text">Profile</p></a></li>
                        <li><a href="#">Another action</a></li>
                        <li><a href="#">Something else here</a></li>
                        <li class="divider"></li>
                        <li><a href="/users/logout">Logout</a></li>
                    </ul>
                </li>
            </ul>
            @endif
        </div>
    </div>
</div>
