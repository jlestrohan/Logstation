@extends('layouts.master')

@section('content')

<div class="bs-docs-header" id="content">
    <div class="container">
        <h1>Settings</h1>

        <p>Please set-up the application parameters here... </p>

    </div>
</div>

<div class="container">
    {{ Form::model($server, array('route' => 'saveSettings', 'class' => 'form-horizontal')) }}



    <div class="panel panel-primary">
        <div class="panel-body">
            <div class="row">

                <div class="col-md-8">

                    <div class="form-group">
                        {{ Form::label('lang','Language',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::select('lang', ['French', 'English'], null, ['class' => 'form-control']) }}
                            {{ $errors->first('lang') }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('dbtype','DB type',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::select('dbtype', ['MySQL'], null, ['class' => 'form-control']) }}
                            {{ $errors->first('dbtype') }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('dbserver','Server',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('dbserver',$server->dbserver, array('class' => 'form-control')) }}
                            {{ $errors->first('dbserver') }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('dbname','DB name',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('dbname',$server->dbname, array('class' => 'form-control')) }}
                            {{ $errors->first('dbname') }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('dbuser','DB user',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::text('dbuser',$server->dbuser, array('class' => 'form-control')) }}
                            {{ $errors->first('dbuser') }}
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('dbpass','DB pass',array('class'=>'col-sm-2 control-label')) }}
                        <div class="col-sm-10">
                            {{ Form::password('dbpass', array('class' => 'form-control')) }}
                            {{ $errors->first('dbpass') }}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                </div>

            </div>
        </div>

        <div class="panel-footer">{{ Form::submit('Save settings',['class'=>'btn btn-success']) }}</div>

    </div>
    {{ Form::close() }}
</div>


@stop