<?php

class UserTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Uncomment the below to wipe the table clean before populating
        //DB::table('users')->truncate();

        $user = array(
            'username' => 'admin',
            'password' => '$2y$10$X67F/j5tVo7791r8/E8ifONeh/ddpIE9LpDg9XCtUL9E4i1AjBD5a',
            'confirmation_code' => '4e8a14eeeb7d95ec5d3b50e95b5f934a',
            'confirmed' => 1,
            'email' => 'aez0309@gmail.com',
            'created_at' => DB::raw('NOW()'),
            'updated_at' => DB::raw('NOW()'),
        );

        // Uncomment the below to run the seeder
        DB::table('users')->insert($user);
    }

}